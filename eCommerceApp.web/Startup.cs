﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eCommerceApp.web.Startup))]
namespace eCommerceApp.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
